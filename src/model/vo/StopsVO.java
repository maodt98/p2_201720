package model.vo;

import model.data_structures.LinearProbingHashST;

public class StopsVO {

	private int stop_id;
	private Integer stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private String zone_id;
 	private String stop_url;
	private byte location_type;
	private String parent_station;
	private int vecesRetrasado;
	private boolean estaRetrasado;
	private int tiempoRetraso;
	private LinearProbingHashST<String, Stops_Estim_ServiceVO> stopsEService;
	
	public StopsVO(int stop_id, Integer stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon,
			String zone_id, String stop_url, byte location_type, String parent_station) {
		super();
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.zone_id = zone_id;
		this.stop_url = stop_url;
		this.location_type = location_type;
		this.parent_station = parent_station;
		this.vecesRetrasado = 0;
		this.setEstaRetrasado(false);
		this.setTiempoRetraso(0);
		this.setStopsEService(new LinearProbingHashST<String, Stops_Estim_ServiceVO>());
	}

	public int getStop_id() {
		return stop_id;
	}

	public void setStop_id(int stop_id) {
		this.stop_id = stop_id;
	}

	public Integer getStop_code() {
		return stop_code;
	}

	public void setStop_code(Integer stop_code) {
		this.stop_code = stop_code;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	public String getStop_desc() {
		return stop_desc;
	}

	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}

	public double getStop_lat() {
		return stop_lat;
	}

	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}

	public double getStop_lon() {
		return stop_lon;
	}

	public void setStop_lon(double stop_lon) {
		this.stop_lon = stop_lon;
	}

	public String getZone_id() {
		return zone_id;
	}

	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	public String getStop_url() {
		return stop_url;
	}

	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}

	public byte getLocation_type() {
		return location_type;
	}

	public void setLocation_type(byte location_type) {
		this.location_type = location_type;
	}

	public String getParent_station() {
		return parent_station;
	}

	public void setParent_station(String parent_station) {
		this.parent_station = parent_station;
	}
	
	public int darVecesRetardado(){
		return this.vecesRetrasado;
	}
	
	public void aumentarVeces(){
		vecesRetrasado++;
	}

	/**
	 * @return the estaRetrasado
	 */
	public boolean isEstaRetrasado() {
		return estaRetrasado;
	}

	/**
	 * @param estaRetrasado the estaRetrasado to set
	 */
	public void setEstaRetrasado(boolean estaRetrasado) {
		this.estaRetrasado = estaRetrasado;
	}

	/**
	 * @return the stopsEService
	 */
	public LinearProbingHashST<String, Stops_Estim_ServiceVO> getStopsEService() {
		return stopsEService;
	}

	/**
	 * @param stopsEService the stopsEService to set
	 */
	public void setStopsEService(LinearProbingHashST<String, Stops_Estim_ServiceVO> stopsEService) {
		this.stopsEService = stopsEService;
	}

	/**
	 * @return the tiempoRetraso
	 */
	public int getTiempoRetraso() {
		return tiempoRetraso;
	}

	/**
	 * @param tiempoRetraso the tiempoRetraso to set
	 */
	public void setTiempoRetraso(int tiempoRetraso) {
		this.tiempoRetraso = tiempoRetraso;
	}
	
}
