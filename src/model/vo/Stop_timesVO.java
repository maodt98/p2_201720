package model.vo;

public class Stop_timesVO {

	private Integer trip_id;
	private String arrival_time;
	private String departure_time;
	private Integer stop_id; 
	private Integer stop_sequence;  
	private String stop_headsign;
	private byte pickup_type;
	private byte drop_off_type;
	private double shape_dist_traveled;
	private StopsVO stop;
	private boolean recorrida;
	private int idHora;
	
	public Stop_timesVO(int trip_id, String arrival_time, String departure_time, Integer stop_id, Integer stop_sequence,
			String stop_headsign, byte pickup_type, byte drop_off_type, double shape_dist_traveled) {
		super();
		this.trip_id = trip_id;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
		this.stop_id = stop_id;
		this.stop_sequence = stop_sequence;
		this.stop_headsign = stop_headsign;
		this.pickup_type = pickup_type;
		this.drop_off_type = drop_off_type;
		this.shape_dist_traveled = shape_dist_traveled;
		this.stop = null;
		this.setStop(null);
		this.recorrida = false;
		this.setIdHora(-1);
		setHoraId();
	}

	public Integer getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}

	public String getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}

	public String getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}

	public Integer getStop_id() {
		return stop_id;
	}

	public void setStop_id(Integer stop_id) {
		this.stop_id = stop_id;
	}

	public Integer getStop_sequence() {
		return stop_sequence;
	}

	public void setStop_sequence(Integer stop_sequence) {
		this.stop_sequence = stop_sequence;
	}

	public String getStop_headsign() {
		return stop_headsign;
	}

	public void setStop_headsign(String stop_headsign) {
		this.stop_headsign = stop_headsign;
	}

	public byte getPickup_type() {
		return pickup_type;
	}

	public void setPickup_type(byte pickup_type) {
		this.pickup_type = pickup_type;
	}

	public byte getDrop_off_type() {
		return drop_off_type;
	}

	public void setDrop_off_type(byte drop_off_type) {
		this.drop_off_type = drop_off_type;
	}

	public double getShape_dist_traveled() {
		return shape_dist_traveled;
	}

	public void setShape_dist_traveled(double shape_dist_traveled) {
		this.shape_dist_traveled = shape_dist_traveled;
	}

	/**
	 * @return the stop
	 */
	public StopsVO getStop() {
		return stop;
	}

	/**
	 * @param stop the stop to set
	 */
	public void setStop(StopsVO stop) {
		this.stop = stop;
	}
	public boolean darRecorrida()
	{
		return recorrida;
	}
	public void marcarRecorrida()
	{
		this.recorrida = true;
	}

	/**
	 * @return the idHora
	 */
	public int getIdHora() {
		return idHora;
	}

	/**
	 * @param idHora the idHora to set
	 */
	public void setIdHora(int idHora) {
		this.idHora = idHora;
	}
	
	public void setHoraId(){
		
		String hora = arrival_time.replaceAll("\\s+","");
		String[] partes = hora.split(":");

		int hInt = Integer.parseInt(partes[0]);
		
		if(hInt == 0) setIdHora(0);
		else if(hInt > 23) setIdHora(hInt - 24);
		else setIdHora(hInt);

	}
}
