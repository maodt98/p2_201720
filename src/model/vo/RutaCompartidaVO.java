package model.vo;

import model.data_structures.LinkedList;

public class RutaCompartidaVO {

	private Integer routeId;
	
	private LinkedList<TripsVO> trips;

	public RutaCompartidaVO(Integer ruta) {
		super();
		this.setRouteId(ruta);
		this.setTrips(new LinkedList<TripsVO>());
	}

	
	/**
	 * @return the routeId
	 */
	public Integer getRouteId() {
		return routeId;
	}


	/**
	 * @param routeId the routeId to set
	 */
	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}


	/**
	 * @return the trips
	 */
	public LinkedList<TripsVO> getTrips() {
		return trips;
	}

	/**
	 * @param trips the trips to set
	 */
	public void setTrips(LinkedList<TripsVO> trips) {
		this.trips = trips;
	}
}
