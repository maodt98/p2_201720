package model.vo;

public class TransfersVO {

	private int from_stop_id;
	private int to_stop_id;
	private byte transfer_type;
	private short min_transfer_time;
	
	public TransfersVO(int from_stop_id, int to_stop_id, byte transfer_type, short min_transfer_time) {
		super();
		this.from_stop_id = from_stop_id;
		this.to_stop_id = to_stop_id;
		this.transfer_type = transfer_type;
		this.min_transfer_time = min_transfer_time;
	}

	public int getFrom_stop_id() {
		return from_stop_id;
	}

	public void setFrom_stop_id(int from_stop_id) {
		this.from_stop_id = from_stop_id;
	}

	public int getTo_stop_id() {
		return to_stop_id;
	}

	public void setTo_stop_id(int to_stop_id) {
		this.to_stop_id = to_stop_id;
	}

	public byte getTransfer_type() {
		return transfer_type;
	}

	public void setTransfer_type(byte transfer_type) {
		this.transfer_type = transfer_type;
	}

	public short getMin_transfer_time() {
		return min_transfer_time;
	}

	public void setMin_transfer_time(short min_transfer_time) {
		this.min_transfer_time = min_transfer_time;
	}
	
}
