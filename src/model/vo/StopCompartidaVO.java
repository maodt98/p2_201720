package model.vo;

import model.data_structures.LinkedList;

public class StopCompartidaVO {

	private StopsVO stop;
	private LinkedList<RutaCompartidaVO> rutas;
	
	public StopCompartidaVO(StopsVO st){
		this.setStop(st);
		this.setRutas(new LinkedList<RutaCompartidaVO>());
	}

	/**
	 * @return the stop
	 */
	public StopsVO getStop() {
		return stop;
	}

	/**
	 * @param stop the stop to set
	 */
	public void setStop(StopsVO stop) {
		this.stop = stop;
	}

	/**
	 * @return the rutas
	 */
	public LinkedList<RutaCompartidaVO> getRutas() {
		return rutas;
	}

	/**
	 * @param rutas the rutas to set
	 */
	public void setRutas(LinkedList<RutaCompartidaVO> rutas) {
		this.rutas = rutas;
	}

}
