package model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteMapVO {
	@SerializedName("Href")
	@Expose
	private String href;

	public RouteMapVO(String href) {
		super();
		this.href = href;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

}

