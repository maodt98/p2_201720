package model.vo;

import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;

public class VORangoHora {
	
	private int id;
	
	private String rangoHora;
	
	private int nRetardos;
	
	/**
	 * Lista de retardos ordenados por tiempo total de retardo
	 */
	private LinearProbingHashST<Integer,TripsVO> listaRetardos;

	public VORangoHora(int id) {
		super();
		this.setId(id);
		this.setRangoHora("");
		this.setnRetardos(0);
		this.listaRetardos = new LinearProbingHashST<Integer, TripsVO>();
		horaSegunId();
	}

	/**
	 * @return the rangoHora
	 */
	public String getRangoHora() {
		return rangoHora;
	}

	/**
	 * @param rangoHora the rangoHora to set
	 */
	public void setRangoHora(String rangoHora) {
		this.rangoHora = rangoHora;
	}

	
	public LinearProbingHashST<Integer, TripsVO> getListaRetardos() {
		return listaRetardos;
	}

	public void setListaRetardos(LinearProbingHashST<Integer, TripsVO> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public void horaSegunId(){
		if(id == 0) setRangoHora("0:00-1:00");
		else if(id == 1) setRangoHora("1:00-2:00");
		else if(id == 2) setRangoHora("2:00-3:00");
		else if(id == 3) setRangoHora("3:00-4:00");
		else if(id == 4) setRangoHora("4:00-5:00");
		else if(id == 5) setRangoHora("5:00-6:00");
		else if(id == 6) setRangoHora("6:00-7:00");
		else if(id == 7) setRangoHora("7:00-8:00");
		else if(id == 8) setRangoHora("8:00-9:00");
		else if(id == 9) setRangoHora("9:00-10:00");
		else if(id == 10) setRangoHora("10:00-11:00");
		else if(id == 11) setRangoHora("11:00-12:00");
		else if(id == 12) setRangoHora("12:00-13:00");
		else if(id == 13) setRangoHora("13:00-14:00");
		else if(id == 14) setRangoHora("14:00-15:00");
		else if(id == 15) setRangoHora("15:00-16:00");
		else if(id == 16) setRangoHora("16:00-17:00");
		else if(id == 17) setRangoHora("17:00-18:00");
		else if(id == 18) setRangoHora("18:00-19:00");
		else if(id == 19) setRangoHora("19:00-20:00");
		else if(id == 20) setRangoHora("20:00-21:00");
		else if(id == 21) setRangoHora("21:00-22:00");
		else if(id == 22) setRangoHora("22:00-23:00");
		else if(id == 23) setRangoHora("23:00-24:00");
	}

	

	/**
	 * @return the nRetardos
	 */
	public int getnRetardos() {
		return nRetardos;
	}

	/**
	 * @param nRetardos the nRetardos to set
	 */
	public void setnRetardos(int nRetardos) {
		this.nRetardos = nRetardos;
	}
	
	public Integer horaAIntSeg(String hora){
		hora = hora.replaceAll("\\s+","");
		String[] partesH = hora.split(":");
		Integer horaA = Integer.parseInt(partesH[0])*3600;
		Integer min = Integer.parseInt(partesH[1])*60;
		Integer seg = 0;
		if(partesH.length>2 )seg = Integer.parseInt(partesH[2]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}
	
	public void sumarRetardos(){
		setnRetardos(nRetardos + 1);
	}

	

}
