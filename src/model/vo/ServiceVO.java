package model.vo;

public class ServiceVO {
	/**
	 * Modela el id del servicio
	 */
	private Integer serviceId;

	/**
	 * Modela la sistancia recorrida del servicio dado una fecha (req 2C)
	 */
	private Double distanciaRecorrida;
	
	public ServiceVO(Integer service){
		this.serviceId = service;
		this.distanciaRecorrida = 0.0;
	}

	/**
	 * @return the serviceId
	 */
	public Integer getServiceId() 
	{
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(Integer serviceId) 
	{
		this.serviceId = serviceId;
	}

	/**
	 * @return the distanciaRecorrida
	 */
	public Double getDistanciaRecorrida()
	{
		return distanciaRecorrida;
	}

	/**
	 * @param distanciaRecorrida the distanciaRecorrida to set
	 */
	public void setDistanciaRecorrida(double distanciaRecorrida)
	{
		this.distanciaRecorrida = distanciaRecorrida;
	}
	
	public void sumDist(double dist){
		distanciaRecorrida += dist;
	}
}
