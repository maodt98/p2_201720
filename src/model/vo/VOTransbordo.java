package model.vo;

import model.data_structures.IList;

public class VOTransbordo
{
	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Id de la parada de origen.
	 */
	private Integer idParadaOrigen;
	


	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	

	public Integer getIdParadaOrigen() {
		return idParadaOrigen;
	}

	public void setIdParadaOrigen(Integer idParadaOrigen) {
		this.idParadaOrigen = idParadaOrigen;
	}

	


}
