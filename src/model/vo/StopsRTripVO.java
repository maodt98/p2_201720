package model.vo;

import model.data_structures.RedBlackBST;

public class StopsRTripVO {

	private int tripId;

	private RedBlackBST<Integer, StopsVO> paradas;
	
	public StopsRTripVO(int tripId) {
		super();
		this.setTripId(tripId);
		this.setParadas(new RedBlackBST<Integer, StopsVO>());
	}

	/**
	 * @return the tripId
	 */
	public int getTripId() {
		return tripId;
	}

	/**
	 * @param tripId the tripId to set
	 */
	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	/**
	 * @return the paradas
	 */
	public RedBlackBST<Integer, StopsVO> getParadas() {
		return paradas;
	}

	/**
	 * @param paradas the paradas to set
	 */
	public void setParadas(RedBlackBST<Integer, StopsVO> paradas) {
		this.paradas = paradas;
	}
}
