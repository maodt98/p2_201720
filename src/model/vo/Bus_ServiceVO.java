package model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bus_ServiceVO {

	@SerializedName("VehicleNo")
	@Expose
	private String vehicleNo;
	@SerializedName("TripId")
	@Expose
	private Integer tripId;
	@SerializedName("RouteNo")
	@Expose
	private String routeNo;
	@SerializedName("Direction")
	@Expose
	private String direction;
	@SerializedName("Destination")
	@Expose
	private String destination;
	@SerializedName("Pattern")
	@Expose
	private String pattern;
	@SerializedName("Latitude")
	@Expose
	private Double latitude;
	@SerializedName("Longitude")
	@Expose
	private Double longitude;
	@SerializedName("RecordedTime")
	@Expose
	private String recordedTime;
	@SerializedName("RouteMap")
	@Expose
	private RouteMapVO routeMap;

	/**
	 * 
	 * @param pattern
	 * @param recordedTime
	 * @param direction
	 * @param tripId
	 * @param routeMap
	 * @param longitude
	 * @param latitude
	 * @param vehicleNo
	 * @param destination
	 * @param routeNo
	 */
	public Bus_ServiceVO(String vehicleNo, Integer tripId, String routeNo, String direction, String destination, String pattern, Double latitude, Double longitude, String recordedTime, RouteMapVO routeMap) {
		super();
		this.vehicleNo = vehicleNo;
		this.tripId = tripId;
		this.routeNo = routeNo;
		this.direction = direction;
		this.destination = destination;
		this.pattern = pattern;
		this.latitude = latitude;
		this.longitude = longitude;
		this.recordedTime = recordedTime;
		this.routeMap = routeMap;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getTripId() {
		return tripId;
	}

	public void setTripId(Integer tripId) {
		this.tripId = tripId;
	}

	public String getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getRecordedTime() {
		return recordedTime;
	}

	public void setRecordedTime(String recordedTime) {
		this.recordedTime = recordedTime;
	}

	public RouteMapVO getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(RouteMapVO routeMap) {
		this.routeMap = routeMap;
	}

}
