package model.vo;

import model.data_structures.IList;

public class VOTransfer
{
	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Id de la parada de origen.
	 */
	private Integer idParadaOrigen;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IList<StopsVO> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public IList<StopsVO> getListadeParadas()
	{
		return listadeParadas;
	}

	public Integer getIdParadaOrigen() {
		return idParadaOrigen;
	}

	public void setIdParadaOrigen(Integer idParadaOrigen) {
		this.idParadaOrigen = idParadaOrigen;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IList<StopsVO> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}
	

}
