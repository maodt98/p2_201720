package model.view;

import java.util.Scanner;

import model.controller.Controller;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;
import model.vo.Bus_ServiceVO;
import model.vo.ParadaViajeVO;
import model.vo.RoutesVO;
import model.vo.RutaCompartidaVO;
import model.vo.StopCompartidaVO;
import model.vo.StopsRTripVO;
import model.vo.StopsVO;
import model.vo.TripsVO;
import model.vo.VORangoHora;
import model.vo.VOTransbordo;

public class View {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			//1C
			case 1:

				//id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase3 = sc.next();
				System.out.println("Ingerese la fecha deseada");
				String fecha = sc.next();

				//TODO  REQUERIMIENTO 1A 
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return IList<VOViaje>
				 */
				RedBlackBST<Integer, TripsVO> viajes = Controller.ITSviajesHuboRetardoParadas1A(Integer.valueOf(idRouteCase3), Integer.valueOf(fecha));
				System.out.println("Los viajes en los que hubo retardo en al menos una parada son:\n");
				for(Integer i:viajes.keys())
				{
					System.out.println(i.toString());
				}
				break;

				//2A
			case 2:
				//fecha
				//n
				System.out.println("Ingrese la fecha");
				String fe = sc.next();	
				//TODO  REQUERIMIENTO 2A 
				/**
				 * Metodo de Controller
				 * @param int n
				 * @return IList<VOParada>
				 */
				RedBlackBST<Integer, StopsVO> s = Controller.ITSparadasMasRetar2A(Integer.valueOf(fe));
				System.out.println("Las paradas en las que hubo m�s retados en la fecha dada son:\n");
				for(Integer h:s.keys())
				{
					StopsVO par = s.get(h);
					System.out.println(par.getStop_id() + "Con tiempo de retardo" + h);
				}

				break;

				//3A
			case 3:

				//id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase5 = sc.next();
				System.out.println("Ingrese la fecha deseada");
				String fechaCase3 = sc.next();

				//TODO  REQUERIMIENTO 3A 
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return IList<VOTransbordo>
				 */
				LinkedList<VOTransbordo> tra = Controller.ITStransbordosRutas3A(Integer.valueOf(idRouteCase5), Integer.valueOf(fechaCase3));
				System.out.println("Se puede hacer transbordo en las paradas:\n");
				Node<VOTransbordo> act = tra.getHead();
				while(act !=null)
				{
					VOTransbordo obj = act.getData();
					System.out.println(obj.getIdParadaOrigen());
					act = act.getNext();
				}


				break;

				//1B
			case 4:

				//Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase4 = sc.next();
				Integer ruta4 = Integer.parseInt(idRutaCase4);
				
				System.out.println("Ingrese la fecha deseada");
				String fechaCase4 = sc.next();
				Integer fecha4 = Integer.parseInt(fechaCase4);

				RedBlackBST<Integer, StopsRTripVO> arbol4 = Controller.viajesRetrasoTotalRuta(ruta4, fecha4);
				if(arbol4 != null ){
					for(Integer i : arbol4.keys()){
						System.out.println("TripId: " + i+ "    " + "Numero de paradas retrasadas: "+ arbol4.get(i).getParadas().size());
					}
				}
				else System.out.println("Datos Insuficientes");

				break;

				//2B
			case 5:
				//Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase5 = sc.next();
				Integer idRuta5 = Integer.parseInt(idRutaCase5);
				
				System.out.println("Ingrese la fecha deseada");
				String fechaCase5 = sc.next();
				Integer fecha5 = Integer.parseInt(fechaCase5);

				RedBlackBST<Integer, VORangoHora> arbol5 = Controller.retardoHoraRuta(idRuta5, fecha5);
				VORangoHora mayor = arbol5.get(arbol5.max());
				System.out.println("Franja de hora con m�s retardos: " + mayor.getRangoHora());
				System.out.println("Numero de paradas retardadas: " + mayor.getnRetardos());
				System.out.println("Numero de viajes retardados: " + mayor.getListaRetardos().size());
				/**for(Integer i : arbol5.keys()){
					VORangoHora act5 = arbol5.get(i);
					System.out.println(act5.getRangoHora() + "  " + act5.getnRetardos());
				}**/

				break;

				//3B
			case 6:

				System.out.println("Ingrese el id de la parada origen");
				String idOrigenCase6 = sc.next();
				Integer idOrigen6 = Integer.parseInt(idOrigenCase6);
				//Id ruta destino
				System.out.println("Ingrese el id de la parada destino");
				String idDestinoCase6 = sc.next();
				Integer idDestino6 = Integer.parseInt(idDestinoCase6);
				//Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase6 = sc.next();
				Integer fecha6 = Integer.parseInt(fechaCase6);
				//Hora de inicio
				System.out.println("Ingrese la hora inicial");
				String horaInicio6 = sc.next();
				//Hora de inicio
				System.out.println("Ingrese la hora final");
				String horaFin6 = sc.next();
				
				RedBlackBST<Integer, ParadaViajeVO> arbol6 = Controller.buscarViajesParadas(idOrigen6, idDestino6, fecha6, horaInicio6, horaFin6);
				if(arbol6.size()>0){
					for(Integer i : arbol6.keys()){
						ParadaViajeVO actual6 = arbol6.get(i);
						System.out.println("RouteId: "+actual6.getRouteId()+"  "+"TripId: "+actual6.getTripId()+"  "+"Tiempo total del viaje: "+actual6.getTiempo()/60+" minutos");
					}
				}
				else System.out.println("No hay viajes que cumplan con datos dados");
				
				break;

				//1C
			case 7:
				System.out.println("Ingrese la fecha");
				String fecha7 = sc.next();
				Controller.cargar(fecha7);
				break;

				//2C
			case 8:

				//n 
				System.out.println("Ingrese la fecha deseada:" );
				String f = sc.next();
				//TODO REQUERIMIENTO 2C
				/**
				 * Metodo de Controller
				 * @param int n
				 * @return IList<VOViaje>
				 */
				RedBlackBST<Integer, TripsVO> j = Controller.ITSNViajesMasDistancia2C(Integer.valueOf(f));
				System.out.println("Los viajes que recorren m�s distancia en la fecha dada son:\n");
				for(Integer u: j.keys())
				{
					TripsVO c = j.get(u);
					System.out.println(c.getTrip_id());
				}


				break;

				//3C
			case 9:

				//Id viaje
				System.out.println("Ingrese el id del viaje deseado:");
				String idViajeCase10 = sc.next();
				System.out.println("Ingrese la fecha deseada");
				String fechaCase9 = sc.next();


				//TODO REQUERIMIENTO 3C
				/**
				 * Metodo de Controller
				 * @param String idViaje
				 * @return IRedBlackBST<Integer, IList<VORetardo>>
				 */
				RedBlackBST<Integer, StopsVO> t = Controller.ITSRetardosViajeFecha3C(Integer.valueOf(idViajeCase10), Integer.valueOf(fechaCase9));
				System.out.println("El viaje presenta retardo, en la fecha dada, en las paradas:\n");
				for(Integer k: t.keys())
				{
					StopsVO pard = t.get(k);
					System.out.println(pard.getStop_id());


				}
				break;

				//4C
			case 10:

				//Id parada
				System.out.println("Ingerse el id de la parada deseada");
				String stopCase10 = sc.next();
				Integer idStop10 = Integer.parseInt(stopCase10);

				System.out.println("Ingrese la fecha");
				String fechaCase10 = sc.next();
				Integer fecha10 = Integer.parseInt(fechaCase10);

				StopCompartidaVO parada10 = Controller.paradaCompartida(idStop10, fecha10);
				if(parada10 == null) {
					System.out.println("La parada no es compartida");
					break;}

				if(parada10.getRutas().getSize() > 1){
					System.out.println("La parada "+ parada10.getStop().getStop_id() +" si es compartida\n");
					for(RutaCompartidaVO i : parada10.getRutas()){
						System.out.println("IdRuta: "+ i.getRouteId() );
						System.out.println("Trips:");
						for(TripsVO z : i.getTrips()){
							System.out.println(z.getTrip_id());
						}
						System.out.println("\n");
					}
				}
				else
					System.out.println("La parada "+ parada10.getStop().getStop_id()+ " no es compartida");

				break;

				//5C
			case 11:

				//Ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String rutaCase12 = sc.next();
				Integer ruta12 = Integer.parseInt(rutaCase12);

				System.out.println("Ingrese la fecha");
				String fechaCase12 = sc.next();
				Integer fecha12 = Integer.parseInt(fechaCase12);

				//HoraInicio
				System.out.println("Ingrese la hora inicial en formato 23:50 ");
				String horaInicio12 = sc.next();

				//HoraFinal
				System.out.println("Ingrese la hora final en formato 23:50 ");
				String horaFinal12 = sc.next();

				RedBlackBST<Integer, LinkedList<StopsVO>> arbol11 = Controller.viajesPararonEnRango(ruta12, fecha12, horaInicio12, horaFinal12);
				if(arbol11 != null){
					for(Integer i : arbol11.keys()){
						System.out.println("TripId: "+i+"     "+ "Numero de paradas: "+ arbol11.get(i).getSize());
					}
				}
				else System.out.println("Datos Insuficientes");

				break;

				//SALIR
			case 12:
				fin=true;
				sc.close();
				break;
			}
		}
	}
	public static void printMenu(){
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");

		System.out.println("Parte A:");
		System.out.println("1. (1A) Dada una ruta  y una fecha,  identificar todos los viajes en los que hubo un retardo en al menos una parada.");
		System.out.println("2. (2A) Identificar  las n paradas en las que hubo más retardos en una fecha dada, teniendo en cuenta todas las rutas que utilizan esa parada.");
		System.out.println("3. (3A) Para una ruta dada en una fecha dada, retornar una lista ordenada  (por tiempo total de viaje) de todos los transbordos posibles, a  otras rutas, a partir de las paradas de dicha ruta.  \n");

		System.out.println("Parte B: ");
		System.out.println("4. (1B) Dada una ruta y una fecha, identificar todos los  viajes en los que después  de un retardo, todas las paradas siguientes tuvieron retardo. Retornar lista ordenada descendentemente por id de viaje.");
		System.out.println("5. (2B) Identificar  la franja de hora entera en la que hubo más paradas con retardos, para una ruta determinada en una fecha dada.");
		System.out.println("6. (3B) Dados los ids de una parada de origen y una parada de destino, buscar los viajes de rutas de bus para ir del origen al destino, en una fecha y franja de horario. \n");

		System.out.println("Parte C:");
		System.out.println("7. Cargar Informaci�n (1C)");
		System.out.println("8.(2C) Identificar  los n viajes que recorren más distancia  en una fecha dada, retirnando una lista ordenada por esta distancia.");
		System.out.println("9.(3C) Retornar un árbol  balanceado de retardos de un viaje (ordenado por tiempo de retardo), dado su  identificador y una fecha  dada.");
		System.out.println("10.(4C) Dada una parada y una fecha, responder si dicha parada es compartida o no en dicha fecha.");
		System.out.println("11.(5C) Dada una ruta, dar todos los viajes (con sus respectivas paradas) que realizaron paradas en un rango de tiempo dado. \n");

		System.out.println("12. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}
