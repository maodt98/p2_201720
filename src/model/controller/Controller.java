package model.controller;

import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MyInteger;
import model.data_structures.RedBlackBST;
import model.logic.STSManager;
import model.vo.Bus_ServiceVO;
import model.vo.ParadaViajeVO;
import model.vo.RoutesVO;
import model.vo.ServiceVO;
import model.vo.StopCompartidaVO;
import model.vo.Stop_timesVO;
import model.vo.StopsRTripVO;
import model.vo.StopsVO;
import model.vo.TripsVO;
import model.vo.VORangoHora;

public class Controller {

	private static STSManager man = new STSManager();
	
	//-------------------------PARTE A--------------------------//
	public static RedBlackBST<Integer, TripsVO> ITSviajesHuboRetardoParadas1A(Integer idRuta, Integer fecha)
	{
		return man.ITSviajesHuboRetardoParadas1A(fecha, idRuta);
	}
	public static RedBlackBST<Integer, StopsVO> ITSparadasMasRetar2A(Integer nFecha)
	{
		return man.ITSparadasMasRetar2A(nFecha);
	}
	public static LinkedList ITStransbordosRutas3A(Integer idRuta, Integer fecha)
	{
		return man.ITStransbordosRuta3A(idRuta, fecha);
	}
	
	//-------------------------PARTE B--------------------------//
	public static RedBlackBST<Integer, StopsRTripVO> viajesRetrasoTotalRuta(Integer pRuta, Integer pFecha){
		return man.viajesRetrasoTotalRuta(pRuta, pFecha);
	}
	
	public static RedBlackBST<Integer, VORangoHora> retardoHoraRuta(int pRuta, Integer pFecha){
		return man.retardoHoraRuta(pRuta, pFecha);
	}
	
	public static RedBlackBST<Integer, ParadaViajeVO> buscarViajesParadas(int pStopOrigen, int pStopDestino, Integer pFecha, String hInicial, String hFinal){
		return man.buscarViajesParadas(pStopOrigen, pStopDestino, pFecha, hInicial, hFinal);
	}
	
	//-------------------------PARTE C--------------------------//
	public static void cargar(String pFecha){
		man.cargar(pFecha);;
	}
	public static RedBlackBST<Integer, TripsVO> ITSNViajesMasDistancia2C(Integer nFecha)
	{
		return man.ITSNViajesMasDistancia2C(nFecha);
	}
	public static RedBlackBST<Integer, StopsVO> ITSRetardosViajeFecha3C(Integer tripID, Integer fecha)
	{
		return man.ITSRetardosViajeFecha3C(tripID, fecha);
	}
	public static StopCompartidaVO paradaCompartida(Integer pStopId, Integer pFecha){
		return man.paradaCompartida(pStopId, pFecha);
	}
	public static RedBlackBST<Integer, LinkedList<StopsVO>> viajesPararonEnRango(int pRuta, Integer pFecha, String horaI, String horaF){
		return man.viajesPararonEnRango(pRuta, pFecha, horaI, horaF);
	}

}
