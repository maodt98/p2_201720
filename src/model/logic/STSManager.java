package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;

import model.data_structures.Heap;
import model.data_structures.IList;
import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;
import model.data_structures.MaxPQ;
import model.data_structures.MyInteger;
import model.data_structures.Node;
import model.data_structures.RadixSort;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHashST;
import model.vo.AgencyVO;
import model.vo.Bus_ServiceVO;
import model.vo.CalendarVO;
import model.vo.Calendar_datesVO;
import model.vo.Feed_infoVO;
import model.vo.ParadaViajeVO;
import model.vo.RoutesVO;
import model.vo.RutaCompartidaVO;
import model.vo.ScheduleVO;
import model.vo.ServiceVO;
import model.vo.ShapesVO;
import model.vo.StopCompartidaVO;
import model.vo.Stop_timesVO;
import model.vo.StopsRTripVO;
import model.vo.StopsVO;
import model.vo.Stops_Estim_ServiceVO;
import model.vo.TransfersVO;
import model.vo.TripsVO;
import model.vo.VORangoHora;
import model.vo.VOTransbordo;

public class STSManager<Key,Value> {
	private LinearProbingHashST<String, AgencyVO> agency = new LinearProbingHashST<>();
	private LinearProbingHashST<Integer, CalendarVO> calendar = new LinearProbingHashST<>();
	private LinearProbingHashST<Integer, RoutesVO> routes = new LinearProbingHashST<>();
	private LinkedList<ShapesVO> shapes = new LinkedList<>();
	//private LinearProbingHashST<String, Stop_timesVO> stopTimes = new LinearProbingHashST<>();
	private LinearProbingHashST<Integer, StopsVO> stops = new LinearProbingHashST<>();
	private LinearProbingHashST<Integer, TripsVO> trips = new LinearProbingHashST<>();
	//private LinearProbingHashST<String, Bus_ServiceVO> busesService = new LinearProbingHashST<>();
	//private LinearProbingHashST<String, Stops_Estim_ServiceVO> stopsEServ = new LinearProbingHashST<>();
	private LinearProbingHashST<Integer,StopsVO> stopsRetardadas = new LinearProbingHashST<>();

	public void cargar(String pFecha) {

		loadAgency();
		System.out.println("---------------Carg� efectivamente Calendar_dates--------------");
		loadCalendar();
		System.out.println("-------------------Carg� efectivamente Feed--------------------");
		loadRoutes();
		System.out.println("------------------Carg� efectivamente Routes-------------------");
		loadShapes();
		System.out.println("------------------Carg� efectivamente Shapes-------------------");
		loadTrips();
		System.out.println("-----------------Carg� efectivamente Trips-----------------");
		loadStops();
		System.out.println("-------------------Carg� efectivamente Stops-------------------");
		loadStopsEstimService(pFecha);
		System.out.println("-----------------Carg� efectivamente StopsEstimService-----------------");
		loadStopTimes();
		System.out.println("-----------------Carg� efectivamente StopTimes-----------------");
		loadBusesService(pFecha);
		System.out.println("-------------------Carg� efectivamente BusesService--------------------");
	}

	public void loadAgency() {
		String file = "./data/agency.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				AgencyVO x = new AgencyVO(datos[0], datos[1], datos[2], datos[3], datos[4]);
				agency.put(datos[0], x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}





	public void loadCalendar() {
		String file = "./data/calendar.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				CalendarVO x = new CalendarVO(Integer.parseInt(datos[0]), Byte.parseByte(datos[1]), Byte.parseByte(datos[2]), Byte.parseByte(datos[3]), Byte.parseByte(datos[4]), Byte.parseByte(datos[5]), Byte.parseByte(datos[6]), Byte.parseByte(datos[7]), Integer.parseInt(datos[8]), Integer.parseInt(datos[9]));

				calendar.put(x.getService_id(),x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}



	public void loadRoutes() {
		String file = "./data/routes.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				RoutesVO x = new RoutesVO(Integer.parseInt(datos[0]), datos[1], datos[2], datos[3], datos[4], Byte.parseByte(datos[5]), datos[6], datos[7], datos[8]);
				routes.put(x.getRoute_id(), x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void loadShapes() {
		String file = "./data/shapes.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				ShapesVO x = new ShapesVO(Integer.parseInt(datos[0]), Double.parseDouble(datos[1]), Double.parseDouble(datos[2]), Short.parseShort(datos[3]), Double.parseDouble(datos[4]));
				shapes.add( x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public void loadStopTimes() {
		String file = "./data/stop_times.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				Stop_timesVO x = new Stop_timesVO(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), datos[5], Byte.parseByte(datos[6]), Byte.parseByte(datos[7]), (datos.length>8)?Double.parseDouble(datos[8]):0);
				x.setStop(stops.get(x.getStop_id()));
				trips.get(x.getTrip_id()).darParadas().put(x.getStop_id(), x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStops() {
		String file = "./data/stops.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				boolean si = false;
				String[] datos = line.split(splitBy);
				StopsVO x = new StopsVO((!datos[0].isEmpty())?Integer.parseInt(datos[0]):0, (!(datos[1].equals(" ")))?Integer.parseInt(datos[1]):-1 , datos[2], datos[3], Double.parseDouble(datos[4]), Double.parseDouble(datos[5]), datos[6], datos[7], (!datos[8].isEmpty())?Byte.parseByte(datos[8]):0, (datos.length>9)?datos[9]:"");
				if(x.getStop_code() != -1){
					stops.put(x.getStop_id(), x);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadTrips() {
		String file = "./data/trips.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				TripsVO x = new TripsVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]), datos[3], datos[4], Byte.parseByte(datos[5]), Integer.parseInt(datos[6]), Integer.parseInt(datos[7]), Byte.parseByte(datos[8]), Byte.parseByte(datos[9]));
				trips.put(x.getTrip_id(), x);
				RoutesVO r = buscarRuta(x.getRoute_id());
				r.darTrips().put(x.getTrip_id(), x);
				r.agregarParadas(x.darParadas());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}



	public void loadBusesService(String pFecha){
		File f;
		if(pFecha.equals("20170821")){
			f = new File("Buses_Service/RealTime-8-21-BUSES_SERVICE");
		}
		else if(pFecha.equals("20170822")){
			f = new File("Buses_Service/RealTime-8-22-BUSES_SERVICE");
		}
		else{
			System.out.println("No existen Buses Services con la fecha dada");
			return;
		}
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readBusesService(updateFiles[i]);
		}
		//PRUEBA
		/**Node<Bus_ServiceVO> act = busesService.getHead();
		while(act != null) {
			System.out.println(act.getData().getDestination());
			act = act.getNext();
		}**/
	}

	public void readBusesService(File f){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
			Gson gson = new GsonBuilder().create();

			Bus_ServiceVO[] busesServicex = gson.fromJson(reader, Bus_ServiceVO[].class);

			for(int i=0; i<busesServicex.length; i++){
				trips.get(busesServicex[i].getTripId()).getBusesServices().put(hora2AIntSeg(busesServicex[i].getRecordedTime()), busesServicex[i]);
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStopsEstimService(String pFecha){
		File f;
		if(pFecha.equals("20170821")){
			f = new File("Stop_Stim_Service/RealTime-8-21-STOPS_ESTIM_SERVICES");
		}
		else if(pFecha.equals("20170822")){
			f = new File("Stop_Stim_Service/RealTime-8-22-STOPS_ESTIM_SERVICES");
		}
		else{
			System.out.println("No existe el Stop Estim Service con la fecha dada");
			return;
		}
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readStopsEstimService(updateFiles[i]);
		}
		//PRUEBA
		/**Node<Stops_Estim_ServiceVO> act = stopsEServ.getHead();
		while(act != null) {
			System.out.println(act.getData().getRouteName());
			act = act.getNext();
		}**/
	}

	public void readStopsEstimService(File f){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
			String linea = reader.readLine();

			if(linea.startsWith("{\"Code\"")){
				return;
			}
			reader = new BufferedReader(new FileReader(f));
			Gson gson = new GsonBuilder().create();
			Stops_Estim_ServiceVO[] stopsEServx = gson.fromJson(reader, Stops_Estim_ServiceVO[].class);

			String[] partes = f.getName().split("_");
			String[] partes2 = partes[3].split("-");
			String[] partes3 = partes2[1].split(".json");

			for(int i=0; i<stopsEServx.length; i++){
				stopsEServx[i].setStop_code(Integer.parseInt(partes2[0]));
				stopsEServx[i].setSequence(Integer.parseInt(partes3[0]));
				for(Integer x : stops.keys()){
					if(stops.get(x).getStop_code().intValue() == stopsEServx[i].getStop_code().intValue()){
						stops.get(x).getStopsEService().put(stopsEServx[i].getSequence()+"$"+stopsEServx[i].getStop_code()+"&"+stopsEServx[i].getRouteNo(), stopsEServx[i]);
						break;
					}
				}
				for(ScheduleVO x : stopsEServx[i].getSchedules())
				{
					if(x.getScheduleStatus().equals("-")){
						StopsVO stop = buscarStopCode(stopsEServx[i].getStop_code()) ;
						if(!stopsRetardadas.contains(stop.getStop_id())){
							stopsRetardadas.put(stop.getStop_id(), stop);}
						else {stop.aumentarVeces();}
					}

				}	

			}


		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}catch(IOException ex){
			ex.printStackTrace();	
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}





	//--------------------------------------------------------------------------------------------------------------
	// Metodos Compartidos
	//--------------------------------------------------------------------------------------------------------------
	/**
	 * Coge una fecha en entero pasada por parametro y la convierte a Date
	 * @param fecha fecha que se quiere pasar a Date
	 * @return fecha en formato Date
	 */
	public Date intADate(Integer fecha){
		Date date = null;
		SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			date = originalFormat.parse(fecha.toString());
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return date;
	}
	/**
	 * Mira la fecha pasada por par�metro e identifica qu� dia de la semana es
	 * @param d Fecha de la que se quiere saber el d�a de la semana
	 * @return Dia de la semana
	 */
	public int getDay(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);	
	}

	/**
	 * Dada una fecha y una ruta por parametro busca los trips
	 * @param fecha fecha de la que se quieren buscar los trips
	 * @param pRuta ruta de la que se quieren saber los trips
	 * @return Retorna una hash con el trip Id como llave y el trip como objeto
	 */
	private LinearProbingHashST<Integer,TripsVO> darTripsConFechaRuta(Integer fecha, int pRuta)
	{

		LinearProbingHashST<Integer,TripsVO> fin = new LinearProbingHashST<Integer,TripsVO>();
		LinearProbingHashST<Integer, TripsVO> lTrips = new LinearProbingHashST<Integer,TripsVO>();
		Iterable<Integer> Tkeys = trips.keys();
		boolean t= false;
		for(Integer cosa:Tkeys)
		{
			TripsVO tAct = trips.get(cosa);
			if(tAct.getRoute_id().intValue() == pRuta){
				lTrips.put(tAct.getTrip_id(), tAct);
			}

		}
		Iterable<Integer>TrKeys = lTrips.keys();
		for(Integer tr:TrKeys)
		{
			TripsVO acTr = trips.get(tr);
			Iterable<Integer> CKeys = calendar.keys();
			for(Integer cal:CKeys){
				CalendarVO actC = calendar.get(cal);
				if(cal.intValue()== acTr.getService_id()){
					boolean si = false;
					int dia = getDay(intADate(fecha));
					if(dia == 1){
						if(actC.getSunday() == 0)
						{fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getMonday() == 0)
						{fin.put(tr, acTr);}
						break;
					}
					else if(dia == 3){
						if(actC.getTuesday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 4){
						if(actC.getWednesday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getThursday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getFriday() == 0){fin.put(tr, acTr);}
						break;
					}
					else{
						if(actC.getSaturday() == 0){fin.put(tr, acTr);}
						break;
					}

				}
			}

		}
		//System.out.println("Hay estos trips en esa fecha"+fin.size());
		return fin;
	}
	/**
	 * Dada una fecha y una ruta por parametro busca los trips
	 * @param fecha fecha de la que se quieren buscar los trips
	 * @param pRuta ruta de la que se quieren saber los trips
	 * @return Retorna una hash con el trip Id como llave y el trip como objeto
	 */
	private LinearProbingHashST<Integer,TripsVO> darTripsConFecha(Integer fecha)
	{

		LinearProbingHashST<Integer,TripsVO> fin = new LinearProbingHashST<Integer,TripsVO>();
		LinearProbingHashST<Integer, TripsVO> lTrips = new LinearProbingHashST<Integer,TripsVO>();
		Iterable<Integer> Tkeys = trips.keys();
		boolean t= false;


		for(Integer tr:Tkeys)
		{
			TripsVO acTr = trips.get(tr);
			Iterable<Integer> CKeys = calendar.keys();
			for(Integer cal:CKeys){
				CalendarVO actC = calendar.get(cal);
				if(cal.intValue()== acTr.getService_id()){
					boolean si = false;
					int dia = getDay(intADate(fecha));
					if(dia == 1){
						if(actC.getSunday() == 0)
						{fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getMonday() == 0)
						{fin.put(tr, acTr);}
						break;
					}
					else if(dia == 3){
						if(actC.getTuesday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 4){
						if(actC.getWednesday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getThursday() == 0){fin.put(tr, acTr);}
						break;
					}
					else if(dia == 2){
						if(actC.getFriday() == 0){fin.put(tr, acTr);}
						break;
					}
					else{
						if(actC.getSaturday() == 0){fin.put(tr, acTr);}
						break;
					}

				}
			}

		}
		return fin;
	}	



	/**
	 * Dice que viajes son los que tienen paradas retardadas
	 * @param fecha Fecha de la que se quieren los viajes que tienen retardos
	 * @param idRuta Ruta de la que se quieren saber los viajes retardados
	 * @return SeparateChaining de viajes retardados
	 */
	private LinearProbingHashST<Integer,TripsVO> viajesRetardados(Integer fecha, Integer idRuta)
	{
		LinearProbingHashST<Integer, TripsVO> trps = darTripsConFechaRuta(fecha, idRuta);
		LinearProbingHashST<Integer,TripsVO> retardados = new LinearProbingHashST<Integer, TripsVO>();
		for(Integer key:trps.keys())
		{
			TripsVO trip = trps.get(key);
			LinearProbingHashST<Integer, Stop_timesVO> stos = trip.darParadas();

			for(Integer ky:stos.keys())
			{
				if(stopsRetardadas.contains(ky))
				{
					retardados.put(key, trip);
					trip.aumentarRet();
					trip.darRetardadas().put(key, stopsRetardadas.get(ky));
				}
			}
		}
		return retardados;
	}
	/**
	 * Dice que viajes son los que tienen paradas retardadas
	 * @param fecha Fecha de la que se quieren los viajes que tienen retardos
	 * @param idRuta Ruta de la que se quieren saber los viajes retardados
	 * @return SeparateChaining de viajes retardados
	 */
	private LinearProbingHashST<Integer,TripsVO> viajesRetardadosFecha(Integer fecha)
	{
		LinearProbingHashST<Integer, TripsVO> trps = darTripsConFecha(fecha);
		LinearProbingHashST<Integer,TripsVO> retardados = new LinearProbingHashST<Integer, TripsVO>();
		for(Integer key:trps.keys())
		{
			TripsVO trip = trps.get(key);
			LinearProbingHashST<Integer, Stop_timesVO> stos = trip.darParadas();

			for(Integer ky:stos.keys())
			{
				if(stopsRetardadas.contains(ky))
				{
					retardados.put(key, trip);
					trip.aumentarRet();
					trip.darRetardadas().put(key, stopsRetardadas.get(ky));
				}
			}
		}
		return retardados;
	}
	/**
	 * Da los trips que tienen retardo en una fecha dada
	 * @param fecha
	 * @return
	 */
	private LinearProbingHashST<Integer, TripsVO> darTripsConRetardoEnFecha(Integer fecha)
	{
		LinearProbingHashST<Integer, TripsVO> retornar = new LinearProbingHashST<>();
		LinearProbingHashST<Integer, TripsVO> enFecha = darTripsConFecha(fecha);
		LinearProbingHashST<Integer, TripsVO> retardados = viajesRetardadosFecha(fecha);
		for(Integer i:enFecha.keys())
		{
			if(retardados.contains(i))
			{
				retornar.put(i, retardados.get(i));
			}
		}
	
		return retornar;
	}
	/**
	 * Actualiza las paradas retardadas de un trip
	 * @param fecha Fecha de la que se quieren los viajes que tienen retardos
	 * @param idRuta Ruta de la que se quieren saber los viajes retardados
	 * @return SeparateChaining de viajes retardados
	 */
	private TripsVO paradasRetardadasFechaTrip(Integer fecha, int idTrip)
	{
		LinearProbingHashST<Integer, TripsVO> trps = trips;//darTripsConFecha(fecha);
		
		TripsVO tripRetar = trps.get(idTrip);
		LinearProbingHashST<Integer, TripsVO>tr = viajesRetardadosFecha(fecha);
		{
			System.out.println();
		}
		LinearProbingHashST<Integer, Stop_timesVO> stos = tripRetar.darParadas();
	
		for(Integer ky:stos.keys())
		{
			Stop_timesVO stop = stos.get(ky);
			StopsVO sto = stops.get(ky);
			if(stopsRetardadas.contains(ky))
			{
				tripRetar.aumentarRet();
				tripRetar.darRetardadas().put(stop.getStop_id(), sto);
			}
		}
		

		return tripRetar;
	}


	private Integer calcularRetardo(RedBlackBST<Integer,Bus_ServiceVO> cosa,StopsVO parada,Stop_timesVO stop)
	{
		Double latP = parada.getStop_lat();
		Double lonP =parada.getStop_lon();
		Integer retornar = 0;
		for(Integer i:cosa.keys())
		{
			Bus_ServiceVO cos = cosa.get(i);
			if(getDistance(cos.getLatitude(), cos.getLongitude(), latP, lonP)<=70)
			{
				retornar = Integer.valueOf(cos.getRecordedTime())-Integer.valueOf(stop.getArrival_time());
					
			}
		}

		return retornar;
	}

	//--------------------------------------------------------------------------------------------------------------------
	// M�todo 1A
	//---------------------------------------------------------------------------------------------------------------------
	/**
	 * Cumple con el requerimiento 1A
	 * @param fecha fecha de la que se quiere saber los viajes retrasados
	 * @param idRuta Ruta de la que se quieren saber los viajes retrasados
	 * @return Retorna un �rbol de trips ordenado por tripID
	 */
	public RedBlackBST<Integer, TripsVO>ITSviajesHuboRetardoParadas1A(Integer fecha, Integer idRuta)
	{
		LinearProbingHashST<Integer, TripsVO> trps = darTripsConFechaRuta(fecha, idRuta);

		RedBlackBST<Integer, TripsVO> retardados = new RedBlackBST<>();

		for(Integer key:trps.keys())
		{
			TripsVO trip = trps.get(key);
			LinearProbingHashST<Integer, Stop_timesVO> stos = trip.darParadas();
			for(Integer ky:stos.keys())
			{
				Stop_timesVO st = stos.get(ky);	
				if(stopsRetardadas.contains(ky))
				{
					retardados.put(key, trip);
					trip.aumentarRet();
					StopsVO ag = stopsRetardadas.get(ky);
					if(trip.getBusesServices().size()!=0){
						Integer retardo = calcularRetardo(trip.getBusesServices(), ag, st);
						trip.agregarRetardada(retardo,ag);}
				}
			}
		}
		System.out.println(retardados.size());
		return retardados;
	}
	private LinearProbingHashST<Integer, Stop_timesVO> darParadasFecha(Integer fecha)
	{
		LinearProbingHashST<Integer, TripsVO> tr = darTripsConFecha(fecha);
		LinearProbingHashST<Integer,Stop_timesVO> st = new LinearProbingHashST<>();
		for(Integer key:tr.keys())
		{
			TripsVO tri = tr.get(key);
			for(Integer o:tri.darParadas().keys())
			{
				Stop_timesVO sto = tri.darParadas().get(o);
				if(!st.contains(sto.getStop_id()))
				{
					st.put(o, sto);
				}
			}
		}
		return st;
	}
	//-------------------------------------------------------------------------------------------------------------
	// M�todo 2A
	//-------------------------------------------------------------------------------------------------------------
	public RedBlackBST<Integer,StopsVO> ITSparadasMasRetar2A(Integer fecha)
	{
		RedBlackBST<Integer,StopsVO> fi = new RedBlackBST<>();
		LinearProbingHashST<Integer, Stop_timesVO> paradas = darParadasFecha(fecha);
		for(Integer k:paradas.keys())
		{
			if(stopsRetardadas.contains(k))
			{
				StopsVO ter = stopsRetardadas.get(k);

				fi.put(ter.darVecesRetardado(), ter);
			}
		}
		return fi;
	}
	//-------------------------------------------------------------------------------------------------------------
	// M�todo 3A
	//-------------------------------------------------------------------------------------------------------------
	public LinkedList<VOTransbordo> ITStransbordosRuta3A( Integer idRuta, Integer fecha) {

		VOTransbordo ayuda = new VOTransbordo();
		LinkedList<VOTransbordo> nueva= new LinkedList<VOTransbordo>();
		LinearProbingHashST<Integer, TripsVO> tablaViajes = darTripsConFechaRuta(fecha, idRuta);
		RoutesVO ruta = null;
		darParadasRutas(fecha);

		for(Integer llave:tablaViajes.keys())
		{
			TripsVO actual=tablaViajes.get(llave);
			ruta = routes.get(actual.getRoute_id());
			ruta.agregarParadas(actual.darParadas());
		}

		RoutesVO otra = routes.get(idRuta);
		LinearProbingHashST<Integer, Stop_timesVO> paradasOtra = otra.darParadas();
		System.out.println("Obveo genio"+paradasOtra.size());

		for(Integer ky:paradasOtra.keys())
		{
			Stop_timesVO s = paradasOtra.get(ky);
			for(Integer k:routes.keys())
			{
				RoutesVO ac = routes.get(k);
				LinearProbingHashST<Integer, Stop_timesVO> paradasEsta = ac.darParadas();
				if(paradasEsta.contains(ky)&&s.darRecorrida()==false)
				{
					Stop_timesVO sd = paradasEsta.get(k);
					
						VOTransbordo ada= new VOTransbordo();
						ada.setIdParadaOrigen(s.getStop_id());
						ada.setTransferTime(ada.getTransferTime()+horaAIntSeg(s.getArrival_time()));
						s.marcarRecorrida();
						nueva.add(ada);
					
				}
			}
		}
		MyInteger<VOTransbordo>[] ordenar = listaArregloTrans(nueva);
		ordenar = RadixSort.sort(ordenar);
		for(int i=0; i<ordenar.length;i++)
		{
			VOTransbordo as = ordenar[i].getData();
			nueva.add(as);
		}


		return nueva;
	}
	public void darParadasRutas(Integer idFecha)
	{
		for(Integer k:routes.keys())
		{
			RoutesVO ruta = routes.get(k);
			LinearProbingHashST<Integer, TripsVO> trps = darTripsConFechaRuta(idFecha, k);
			for(Integer s:trps.keys())
			{
				TripsVO trip = trps.get(s);
				ruta.agregarParadas(trip.darParadas());
				
			}
			
		}
	}
	//---------------------------------------------------------------------------------------------------------
	//Metodo 2C
	//----------------------------------------------------------------------------------------------------------
	public RedBlackBST<Integer, TripsVO> ITSNViajesMasDistancia2C(Integer nFecha)
	{
		RedBlackBST<Integer, TripsVO> retornar = new RedBlackBST<Integer, TripsVO>();
		LinearProbingHashST<Integer, TripsVO> trs = darTripsConFecha(nFecha);
		for(Integer key:trs.keys())
		{
			TripsVO ac = trips.get(key);
			retornar.put(ac.darDistanciaRecorrida().intValue(), ac);
		}
		return retornar;
	}

	//---------------------------------------------------------------------------------------------------------
	//Metodo 3C
	//----------------------------------------------------------------------------------------------------------
	/**
	 * Calcula el retardo de las paradas
	 * @param fecha
	 * @param idRuta
	 * @param stpsRetar
	 * @return
	 * @throws Exception 
	 */
	public RedBlackBST<Integer, StopsVO>  ITSRetardosViajeFecha3C(Integer tripID, Integer fecha) 
	{
		TripsVO trip = paradasRetardadasFechaTrip(fecha, tripID);
	
		RedBlackBST<Integer, StopsVO> nuevo = new RedBlackBST<>();
		RedBlackBST<Integer, StopsVO> retardos = trip.darRetardadas();
		//System.out.println("SIsas"+retardos.size());
		LinearProbingHashST<Integer, Stop_timesVO> paradas = trip.darParadas();
		for(Integer i:retardos.keys())
		{
			StopsVO c = retardos.get(i);
			Stop_timesVO sp =paradas.get(c.getStop_id());
			Integer retardo = calcularRetardo(trip.getBusesServices(), c, sp);
			//System.out.println(c.getTiempoRetraso());
			nuevo.put(c.getTiempoRetraso(), c);
		}
		//System.out.println("Entro a aca sisasssss"+nuevo.size());
		return nuevo;
	}



	private MyInteger<Value>[] listaArreglo(LinkedList pLista){

		MyInteger<Value>[] arr = new MyInteger[pLista.getSize()];
		Node<Value> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<Value> n = new MyInteger<>(i);
			n.setData(act.getData());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}
	private MyInteger<VOTransbordo>[] listaArregloTrans(LinkedList<VOTransbordo> pLista){

		MyInteger<VOTransbordo>[] arr = new MyInteger[pLista.getSize()];
		Node<VOTransbordo> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<VOTransbordo> n = new MyInteger<>(i);
			n.setData(act.getData());
			n.setValue(act.getData().getTransferTime());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}
	private LinkedList<MyInteger<Value>> arregloLista(MyInteger[] pArreglo)
	{
		LinkedList<MyInteger<Value>> retornar = new LinkedList<>();
		for(int i = 0; i<pArreglo.length;i++)
		{
			MyInteger<Value> cosa = pArreglo[i];
			retornar.add(cosa);
		}
		return retornar;
	}

	/**
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */




	public Integer horaAIntSeg(String hora){
		hora = hora.replaceAll("\\s+","");
		String[] partesH = hora.split(":");
		Integer horaA = Integer.parseInt(partesH[0])*3600;
		Integer min = Integer.parseInt(partesH[1])*60;
		Integer seg = 0;
		if(partesH.length>2 )seg = Integer.parseInt(partesH[2]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}

	public Integer hora2AIntSeg(String hora){
		String[] partes = hora.split(":");
		String[] parte3 = partes[2].split("\\s+");

		Integer horaA = Integer.parseInt(partes[0]);
		if(parte3[1].equals("pm")){
			horaA += 12;
		}
		horaA = horaA*3600;
		Integer min = Integer.parseInt(partes[1])*60;
		Integer seg = Integer.parseInt(parte3[0]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}


	//-----------------------------------------------------------------
	//--------------------------Parte Mauricio-------------------------
	//-----------------------------------------------------------------
	public RedBlackBST<Integer, StopsRTripVO> viajesRetrasoTotalRuta(Integer pRuta, Integer pFecha){

		RedBlackBST<Integer, StopsRTripVO> arbol = new RedBlackBST<>();

		LinearProbingHashST<Integer, TripsVO> lTrips = trips; //darTripsConFechaRuta(pFecha, pRuta); 
		boolean hayDatos = false;
		for(Integer i:lTrips.keys()){
			TripsVO t = lTrips.get(i);
			if(t.getBusesServices().size() > 0){
				hayDatos = true;
			}
		}
		if(!hayDatos)return null;

		activarRetrasadas(lTrips);

		for(Integer i : lTrips.keys()){
			TripsVO t = lTrips.get(i);
			for(Integer j : t.darParadas().keys()){
				Stop_timesVO st = t.darParadas().get(j);
				int hora = horaAIntSeg(st.getArrival_time());
				t.getParadasOrdenadas().put(hora, st);
			}
		}

		for(Integer i : lTrips.keys()){
			TripsVO t = trips.get(i);
			StopsRTripVO nuevo = new StopsRTripVO(t.getTrip_id());
			boolean si = false;
			for(Integer j : t.getParadasOrdenadas().keys()){
				Stop_timesVO st = t.getParadasOrdenadas().get(j);
				StopsVO stop = st.getStop();
				if(stop == null) continue;
				if(!si && stop.isEstaRetrasado()){
					si = true;
					nuevo.getParadas().put(stop.getTiempoRetraso(), stop);
				}
				else if(si && stop.isEstaRetrasado()){
					nuevo.getParadas().put(stop.getTiempoRetraso(), stop);
				}
				else if( !si && !stop.isEstaRetrasado()) continue;

				else{
					si = false;
					break;
				}
			}
			if(si && nuevo.getParadas().size() > 1) arbol.put(nuevo.getTripId(), nuevo);
		}
		return arbol;
	}

	public RedBlackBST<Integer, VORangoHora> retardoHoraRuta(int pRuta, Integer pFecha){

		RedBlackBST<Integer, VORangoHora> arbol = new RedBlackBST<>();
		LinearProbingHashST<Integer, VORangoHora> lp = new LinearProbingHashST<>();

		for(int i =0; i<24;i++){
			VORangoHora nuevo = new VORangoHora(i);
			lp.put(i, nuevo);
		}

		LinearProbingHashST<Integer, TripsVO> lTrips = trips; //darTripsConFechaRuta(pFecha, pRuta); 

		boolean hayDatos = false;
		for(Integer i:lTrips.keys()){
			TripsVO t = lTrips.get(i);
			if(t.getBusesServices().size() > 0){
				hayDatos = true;
			}
		}
		if(!hayDatos)return null;

		activarRetrasadas(lTrips);

		for(Integer t : lTrips.keys()){
			TripsVO tAct = lTrips.get(t);
			for(Integer s : tAct.darParadas().keys()){
				Stop_timesVO st = tAct.darParadas().get(s);
				StopsVO stop = st.getStop();
				if(stop != null && stop.isEstaRetrasado()){
					VORangoHora act = lp.get(st.getIdHora());
					act.getListaRetardos().put(tAct.getTrip_id(), tAct);
					act.sumarRetardos();
				}
			}
		}

		for(Integer i : lp.keys()){
			VORangoHora act = lp.get(i);
			arbol.put(act.getnRetardos(), act);
		}

		return arbol;
	}

	public RedBlackBST<Integer, ParadaViajeVO> buscarViajesParadas(int pStopOrigen, int pStopDestino, Integer pFecha, String hInicial, String hFinal){

		RedBlackBST<Integer, ParadaViajeVO> arbol = new RedBlackBST<>(); 
		LinearProbingHashST<Integer, TripsVO> lTrips = darTripsConFecha(pFecha); 
		int hIn = horaAIntSeg(hInicial);
		int hFin = horaAIntSeg(hFinal);

		for(Integer i : lTrips.keys()){
			TripsVO t = lTrips.get(i);
			if(t.darParadas().contains(pStopOrigen) && t.darParadas().contains(pStopDestino)){
				int hora1 = horaAIntSeg(t.darParadas().get(pStopOrigen).getArrival_time());
				int hora2 = horaAIntSeg(t.darParadas().get(pStopDestino).getArrival_time());
				if(hora1 > hora2) continue;
				if(hora1 > hIn && hora2 < hFin){
					ParadaViajeVO nueva = new ParadaViajeVO(t.getTrip_id(), t.getRoute_id());
					int tiempo = hora2 - hora1;
					nueva.setTiempo(tiempo);
					arbol.put(nueva.getTripId(), nueva);
				}
			}
		}
		return arbol;
	}


	public StopCompartidaVO paradaCompartida(Integer pStopId, Integer pFecha){

		LinearProbingHashST<Integer, TripsVO> lTrips = darTripsConFecha(pFecha);
		StopCompartidaVO retorno = new StopCompartidaVO(stops.get(pStopId));
		boolean encontrado = false;
		for(Integer i : lTrips.keys()){
			if(encontrado)break;
			TripsVO tActual = lTrips.get(i);
			for(Integer j: tActual.darParadas().keys()){
				if(encontrado)break;
				Stop_timesVO stActual = tActual.darParadas().get(j);
				if(stActual.getStop_id().intValue() == retorno.getStop().getStop_id()){
					RutaCompartidaVO ruta = new RutaCompartidaVO(tActual.getRoute_id());
					ruta.getTrips().add(tActual);
					retorno.getRutas().add(ruta);
					encontrado = true;
				}
			}
		}

		for(Integer i:lTrips.keys()){
			TripsVO tAct = lTrips.get(i);
			if(tAct.getRoute_id().intValue() != retorno.getRutas().getHead().getData().getRouteId().intValue()){
				boolean encontrado2 = false;
				for(Integer j : tAct.darParadas().keys()){
					if(encontrado2)break;
					Stop_timesVO stActual = tAct.darParadas().get(j);
					if(stActual.getStop_id().intValue() == retorno.getStop().getStop_id()){
						RutaCompartidaVO ruta = new RutaCompartidaVO(tAct.getRoute_id());
						ruta.getTrips().add(tAct);
						retorno.getRutas().add(ruta);
						encontrado2 = true;
					}
				}
			}
		}
		return retorno;
	}

	public RedBlackBST<Integer, LinkedList<StopsVO>> viajesPararonEnRango(int pRuta, Integer pFecha, String horaI, String horaF){

		RedBlackBST<Integer, LinkedList<StopsVO>> arbol = new RedBlackBST<>();
		int hIn = horaAIntSeg(horaI);
		int hFin = horaAIntSeg(horaF);

		LinearProbingHashST<Integer, TripsVO> lTrips = trips; //darTripsConFechaRuta(pFecha, pRuta); 
		boolean hayDatos = false;
		for(Integer i:lTrips.keys()){
			TripsVO t = lTrips.get(i);
			if(t.getBusesServices().size() > 0){
				hayDatos = true;
			}
		}
		if(!hayDatos)return null;

		for(Integer i : lTrips.keys()){
			TripsVO t = lTrips.get(i);
			boolean si = false;
			if(t.getBusesServices()!= null && t.getBusesServices().size() > 0){
				for(Integer j : t.getBusesServices().keys()){
					Bus_ServiceVO bus = t.getBusesServices().get(j);
					double latB = bus.getLatitude();
					double lonB = bus.getLongitude();
					int hBus = hora2AIntSeg(bus.getRecordedTime());
					if(hBus>=hIn && hBus<=hFin){
						boolean encontro = false;
						for(Integer s : t.darParadas().keys()){
							if(encontro)break;
							Stop_timesVO stop = t.darParadas().get(s);
							double latS = stop.getStop().getStop_lat();
							double lonS = stop.getStop().getStop_lon();
							if(getDistance(latB,lonB,latS,lonS)<=70 && !si){
								encontro = true;
								si = true;
								arbol.put(t.getTrip_id(), new LinkedList<StopsVO>());
								arbol.get(t.getTrip_id()).add(stop.getStop());
							}
							else if(si){
								arbol.get(t.getTrip_id()).add(stop.getStop());
							}
						}
					}
				}
			}
		}
		return arbol;
	}



	//--------------------------------------------------------------------
	//-----------------------------SubMetodos-----------------------------
	//--------------------------------------------------------------------

	private double getDistance(double lat1, double lon1, double lat2, double lon2) {

		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	private RoutesVO buscarRuta(Integer ru)
	{
		RoutesVO retornar = null;
		if(routes.contains(ru))
		{
			retornar = routes.get(ru);
		}
		return retornar;
	}
	private StopsVO buscarStopCode(Integer sto)
	{
		StopsVO retornar = null;
		for(Integer key:stops.keys())
		{
			StopsVO actual = stops.get(key);
			if(actual.getStop_code().intValue()==sto.intValue())
			{
				retornar = actual;
			}
		}
		return retornar;
	}

	public void activarRetrasadas(LinearProbingHashST<Integer, TripsVO> pTrips){

		for(Integer i : pTrips.keys()){
			TripsVO t = pTrips.get(i);
			if(t.getBusesServices()!= null && t.getBusesServices().size() > 0){
				Bus_ServiceVO pivote = null;
				for(Integer j : t.getBusesServices().keys()){
					Bus_ServiceVO bus = t.getBusesServices().get(j);
					double latB = bus.getLatitude();
					double lonB = bus.getLongitude();
					int hBus = hora2AIntSeg(bus.getRecordedTime());
					for(Integer s : t.darParadas().keys()){
						Stop_timesVO stopT = t.darParadas().get(s);
						StopsVO stop = stopT.getStop();
						int hStop = horaAIntSeg(stopT.getArrival_time());
						double latS = stop.getStop_lat();
						double lonS = stop.getStop_lon();
						if(getDistance(latB,lonB,latS,lonS)<=70 ){
							if(pivote == null){
								int difHora = hBus - hStop;
								if(difHora > 1200){
									stop.setEstaRetrasado(true);
									stop.setTiempoRetraso(difHora);
								}
							}
							else{
								int hPiv = hora2AIntSeg(pivote.getRecordedTime());
								int hMedia = Integer.valueOf((hBus + hPiv)/2);
								int difHora = hMedia - hStop;
								if(difHora > 1200){
									stop.setEstaRetrasado(true);
									stop.setTiempoRetraso(difHora);
								}
							}
						}
					}
					pivote = bus;
				}
			}
		}
	}







}
