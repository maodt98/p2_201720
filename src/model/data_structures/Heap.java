package model.data_structures;

public class Heap {

	private int[] Heap;
	private int size;
	private int maxTam;

	private static final int FRENTE = 1;

	public Heap(int maxsize)
	{
		this.maxTam = maxsize;
		this.size = 0;
		Heap = new int[this.maxTam + 1];
		Heap[0] = Integer.MAX_VALUE;
	}

	private int padre(int pos)
	{
		return pos / 2;
	}

	private int hijoIzq(int pos)
	{
		return (2 * pos);
	}

	private int hijoDer(int pos)
	{
		return (2 * pos) + 1;
	}

	private boolean esHoja(int pos)
	{
		if (pos >=  (size / 2)  &&  pos <= size)
		{
			return true;
		}
		return false;
	}

	private void swap(int fpos,int spos)
	{
		int tmp;
		tmp = Heap[fpos];
		Heap[fpos] = Heap[spos];
		Heap[spos] = tmp;
	}

	private void maxHeapify(int pos)
	{
		if (!esHoja(pos))
		{ 
			if ( Heap[pos] < Heap[hijoIzq(pos)]  || Heap[pos] < Heap[hijoDer(pos)])
			{
				if (Heap[hijoIzq(pos)] > Heap[hijoDer(pos)])
				{
					swap(pos, hijoIzq(pos));
					maxHeapify(hijoIzq(pos));
				}else
				{
					swap(pos, hijoDer(pos));
					maxHeapify(hijoDer(pos));
				}
			}
		}
	}

	public void insert(int element)
	{
		Heap[++size] = element;
		int current = size;

		while(Heap[current] > Heap[padre(current)])
		{
			swap(current,padre(current));
			current = padre(current);
		}	
	}

	public void maxHeap()
	{
		for (int pos = (size / 2); pos >= 1; pos--)
		{
			maxHeapify(pos);
		}
	}

	public int remove()
	{
		int popped = Heap[FRENTE];
		Heap[FRENTE] = Heap[size--]; 
		maxHeapify(FRENTE);
		return popped;
	}
}
