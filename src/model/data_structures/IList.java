package model.data_structures;

public interface IList <T>
{
    /**
     * Retorna la cabeza de la lista
     * @return Cabeza de la lista
     */
    public Node<T> getHead();

    /**
     * Retorna el ultimo elemento de la lista
     * @return Ultimo elemento de la lista
     */
    public Node<T> getLast();

    /**
     * Retorna el elemento actual
     * @return Elemento actual
     */
    public Node<T> getCurrent();

    /**
     * Retorna el tamanio de la lista
     * @return Tamanio de la lista
     */
    public Integer getSize();

    /**
     * Dice si la lista se encuentra vacía
     * @return True si esta vacia
     * @throws Exception Excepcion si la lista se encuentra vacia
     */
    public boolean isEmpty() throws Exception;

    /**
     * Busca y retorna un elemento de la lista
     * @param nodo Elemento que se quiere encontrar
     * @return Elemento encontrado
     * @throws Exception Excepcion si el elemento no se encuentra en la lista
     */
    public Node<T> getElement(Node<T> nodo) throws Exception;

    /**
     * Retorna el siguiente nodo del actual
     * @return Siguiente nodo
     */
    public Node<T> next();

}
