package model.data_structures;

public class Node<T> {
	/**
	 * Nodo siguiente al actual
	 */
	private Node<T> next;

	private Node<T> prev;

	private T data;
	
	private int priority;

	
	public Node(T element, Node next, Node prev, int p) {
		this.data = element;
		this.next = next;
		this.prev = prev;
		this.priority = p;
	}
	
	public Node(T element, Node next, Node prev){
		this.data = element;
		this.next = next;
		this.prev = prev;
		this.priority = 0;
	}


	public Node(T data) {

		this(data, null, null, 0);

	}
	public Node(T data, int p){
		this(data, null, null, p);
	}

	public Node(T data, Node<T> next) {
		this.setData(data);
		this.next = next;
	}

	/**
	 * Retorna el siguiente elemento
	 * @return Siguiente elemento
	 */
	public Node<T> getNext()

	{
		return next;
	}
	/**
	 * Retorna el siguiente anterior
	 * @return Elemento anterior
	 */
	public Node<T> getPrev()

	{
		return prev;
	}
	
	//------------------------------------------------------------------------------------------------------------------
	//  Modificadores
	//------------------------------------------------------------------------------------------------------------------
	/**
	 * Cambia el siguiente nodo del actual
	 * @param nw Nuevo nodo
	 */
	public void setNext(Node<T> nw)
	{
		next = nw;
	}
	
	/**
	 * Cambia el anterior nodo del actual
	 * @param nw Nuevo nodo
	 */
	public void setPrev(Node<T> nw)
	{
		prev = nw;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}
	

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}


	/**
	 * @return the prioridad
	 */
	public int getPrioridad() {
		return priority;
	}


	/**
	 * @param prioridad the prioridad to set
	 */
	public void setPrioridad(int prioridad) {
		this.priority = prioridad;
	}




}
